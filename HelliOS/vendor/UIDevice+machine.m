//
//  UIDevice+machine.m
//  blind_test
//
//  Created by Olivier THIERRY on 12/10/10.
//  Copyright 2010 Chugulu. All rights reserved.
//

#import "UIDevice+machine.h"
#import "UIDevice+machine.h"

#include <sys/types.h>
#include <sys/sysctl.h>

@implementation UIDevice (machine)

- (NSString *)machine
{
	size_t size;
	sysctlbyname("hw.machine", NULL, &size, NULL, 0); 
	char *name = malloc(size);
	sysctlbyname("hw.machine", name, &size, NULL, 0);
	NSString *machine = [NSString stringWithUTF8String:name];
	free(name);
	
	return machine;
}

@end