/*******************************************************************************
 *  @file		HONotificationCenterHTTPClient.m
 *  @brief		HelliOs 
 *  @author		Sergio Kunats
 *  @version	1.0
 *  @date		7/26/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HelliOS.h"
#import "HONotificationCenterHTTPClientHelliOS.h"

@implementation HONotificationCenterHTTPClientHelliOS

#pragma mark - Delegates
#pragma mark <HONotificationCenterHTTPClientProtocol> methods
- (void) pull {
    HORequest *request          = [HORequest request];
    request.HTTPMethod          = @"GET";
    request.delegate            = self;
    request.URL                 = [self notificationsURL];

    [request startAsynchronously];
}
#pragma mark <HORequestDelegate> methods

- (void) request:(HORequest *)request didSuccessWithObject:(id)object {
    [self deliver:(NSArray*)object];
}

@end
