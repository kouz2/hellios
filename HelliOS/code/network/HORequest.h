/**
 @file		HORequest.h
 @brief		HORequest 
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.
*/

#import <Foundation/Foundation.h>
#ifdef __HO_LIB__
#import "UIDevice+machine.h"
#import "HOEncoder.h"
#import "HODataConverter.h"
#else
#import "../Support/UIDevice+machine.h"
#import "../HOEncoder.h"
#import "../HODataConverter.h"
#endif

@class HORequest;
@class HOResponse;
@class HOHTTPContext;


#pragma mark - Protocol declaration


@protocol HORequestDelegate <NSObject>

@optional

- (void) request:(HORequest*)request didSuccessWithData:(NSData*)result;
- (void) request:(HORequest*)request didSuccessWithObject:(id)object;

- (void) request:(HORequest*)request didReceiveData:(NSData*)data;
- (void) request:(HORequest*)request didReceiveResponse:(HOResponse*)response;
- (void) request:(HORequest*)request didFailWithError:(NSError*)error;

@end


#pragma mark - Interface declarations


@interface HORequest : NSMutableURLRequest 
{	
@private	
	id <HORequestDelegate>       _delegate;
	
	NSDictionary				*_parameters;
	NSString					*_identifier;

    HOHTTPContext               *_context;
    
    BOOL                        _built;
}


#pragma mark - Properties

/**
 * @brief The delegate of the request
 */
@property (nonatomic, retain)   id <HORequestDelegate>		delegate;


/**
 * @brief The parameters of the request, key-value pairs
 */
@property (nonatomic, retain)   NSDictionary				*parameters;

/**
 * @brief the identifier of the request
 *
 * @note  Usefull when you performthe request asynchronouysly so you can
 *        identify it in the delegate callback methods
 */
@property (nonatomic, retain)   NSString                    *identifier;

/**
 * @brief the HTTPContext whitch is attached to the request
 *
 * @see   HOHTTPContext.h/m
 */
@property (nonatomic, assign)   HOHTTPContext               *context;


#pragma mark - Methods


/**
 @brief initialization method
 
 @return id        : the initialized HORequest (autoreleased)
 */
+ (HORequest*)   request;


/**
 @brief this method start loading the request synchronously then returns
        the result as binary (NSData*)
 
 @return NSData    : the initialized HORequest (autoreleased)
 */
- (id)            startSynchronously;


/**
 @brief this method start loading the request asynchronously
 
 @note  the delegate will be notified by the state of the request
 */
- (void)		  startAsynchronously;


/**
 @brief this method build the request. that is to stay it creates its envelop
        encapsuling the parameters, header etc... after that the request is ready to use.
 
 @note  if you're building the request from your own, wheneveryou modify the URL or the paremeters
        your MUST call this method to build the request before using it.
*/
- (void)          build;

/**
 @brief use this method if you want to inject custom data in the user-agent header field
 
 @note  You don't need to call this method if you use HOProvider to build your request
        (assuming you provide the custom header when you have created the HOProvider object)
*/
- (void)          setHeader:(NSDictionary*)header;

@end
