/**
 *  @file		HOHTTPContext.h
 *  @brief		HelliOS 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		6/23/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 */

#import <Foundation/Foundation.h>

#ifdef __HO_LIB__
#import "HORequest.h"
#import "HOResponse.h"
#import "Reachability.h"
#else
#import "HORequest.h"
#import "HOResponse.h"
#import "../Support/Reachability.h"
#endif

extern NSString  *HOHTTPContextDidLoadNotification;
extern NSString  *HOHTTPContextDidFailNotification;
extern NSString  *HOHTTPContextDidCancelNotification;
extern NSString  *HOHTTPContextDidReceiveResponseNotification;


#pragma mark - Interface declaration


@interface HOHTTPContext : NSObject 
{
@private
    HORequest           *_request;
    HOResponse          *_response;
    
    NSMutableData       *_payload;
    NSURLConnection     *_connection;
    BOOL                 _loading;
}


#pragma mark - Properties


/**
 * @brief The request attached to the context
 */
@property (nonatomic, readonly, retain) HORequest       *request;


/**
 * @brief The response loaded in the context
 */
@property (nonatomic, readonly, retain) HOResponse      *response;


/**
 * @brief The payload whitch is loaded from the connection
 */
@property (nonatomic, readonly)         NSMutableData   *payload;


/**
 * @brief Boolean value indicating if the context is currently loading or not
 */
@property (nonatomic, readonly)         BOOL            loading;


#pragma mark - Methods


- (id)   initWithRequest:(HORequest*)request;
+ (id)   HTTPContextWithRequest:(HORequest*)request;

/**
 * @brief Starts loading the context asynchronously
 */
- (void) loadAsynchronously;


/**
 * @brief Starts loading the context synchronously
 */
- (id)   loadSynchronously;


/**
 * @brief cancel the connection and stop loading
 */
- (void) cancel;

@end
