/**
 @file		HOProvider.h
 @brief		HOProvider 
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.
 
 HOProvider object represent an distant provider, typically a server whitch
 communicate with your application.
 It provides methods for performing Asynchronous/Synchronous service calls easily.
 It also provides methods for building requests if you want to do custom things 
 before performing them.
*/

#import "HORequest.h"
#ifdef __HO_LIB__
#import "HODataConverter.h"
#else
#import "../HODataConverter.h"
#endif

#pragma mark - Interface declaration


@interface HOProvider : NSObject {
@private
    NSString        *_name;
    NSString        *_baseURL;
    NSDictionary    *_requestsFormatedHeader;
}


#pragma mark - Properties


/**
 * @brief The provider's name.
 *
 * @note  The name you assign to a provider is used to retreive it
 *        when you ask HelliOS to get it back using [HelliOS providerWithName:@"your_provider_name"]
 */
@property (nonatomic, readonly) NSString        *name;

/**
 * @brief The base URL of your provider object.
 *        Ex : www.blindtest.com
 */
@property (nonatomic, readonly) NSString        *baseURL;


/**
 * @brief Represents a set of key-value pair you want to pass
 *        in your HTTP headers User-Agent in EVERY requests you
 *        perform to this provider. For example you could pass an access token
 *
 * @note  Can be nil
 */
@property (nonatomic, readonly) NSDictionary    *requestsFormatedHeader;


#pragma mark - Methods

/**
 * @param NSArray*              providers : array of HOProvider objects
 */
+ (void) setProviders:(NSArray*)providers;


/**
 @brief this method return the HOProvider object corresponding to a given name
 
 @param NSString*              providerName : the provider name
 */
+ (HOProvider*) providerForName:(NSString*)providerName;

/**
 @brief  initialization method
 
 @param  NSString* name     : the name of the provider
 @param  NSString* baseURL  : the base URL for provider services (e.g: www.blindtest.com)
 
 @return id                 : An initialized HOProvider object 
 
 @note   Neither the name nor the baseURL parameters can be nil. Otherwise HOProviderInitializationException will be raised
*/
- (id) initWithName:(NSString*)name baseServiceURL:(NSString*)baseURL;

/**
 @brief  initialization method
 
 @param  NSString* name           : the name of the provider
 @param  NSString* baseURL        : the base URL for provider services (e.g: www.blindtest.com)
 @param  CGFloat protocolVersion  : the base URL for provider services (e.g: www.blindtest.com)
 
 @return id                 : An initialized HOProvider object 
 
 @note   Neither the name nor the baseURL parameters can be nil. Otherwise HOProviderInitializationException will be raised
 */
- (id) initWithName:(NSString*)name baseServiceURL:(NSString*)baseURL requestsFormatedHeader:(NSDictionary*)header;

/**
 @brief  initialization method
 
 @param  NSString* name     : the name of the provider
 @param  NSString* baseURL  : the base URL for provider services (e.g: www.blindtest.com)
 
 @return id                 : An initialized HOProvider object (autoreleased)
 
 @note   Neither the name nor the baseURL parameters can be nil. Otherwise HOProviderInitializationException will be raised
*/
+ (id) providerWithName:(NSString*)name baseServiceURL:(NSString*)baseURL;

+ (id) providerWithName:(NSString*)name baseServiceURL:(NSString*)baseURL requestsFormatedHeader:(NSDictionary*)header;


/**
 @brief  The method build a HORequest from given parameters
 
 @param  NSString*     path        : the path of the service you wanto call (e.g: @"/user/authenticate")
 @param  NSDictionary* parameters  : the parameters of the request
 @param  NSString*     httpMethod  : the HTTP method to use for the request (default is GET)
 
 @return HORequest              : An initialized and built HORequest (autoreleased) 
*/
- (HORequest*) getRequestForServiceWithPath:(NSString*)path 
                                    parameters:(NSDictionary*)parameters
                                    HTTPMethod:(NSString*)httpMethod;

/**
 @brief  The method build a HORequest from given parameters and perform it asynchronously
         notifying the delegate from its state
 
 @param  NSString*                path        : the path of the service you wanto call (e.g: @"/user/authenticate")
 @param  NSDictionary*            parameters  : the parameters of the request
 @param  NSString*                httpMethod  : the HTTP method to use for the request (default is GET)
 @param  id<HORequestDelegate> delegate    : the delegate of the request
*/
- (void)          performAsynchronousRequestForServiceWithPath:(NSString*)path 
                                                    parameters:(NSDictionary*)parameters
                                                    HTTPMethod:(NSString*)httpMethod
                                                      delegate:(id<HORequestDelegate>)delegate;

/**
 @brief  The method build a HORequest from given parameters and perform it asynchronously
 notifying the delegate from its state
 
 @param  NSString*                path        : the path of the service you wanto call (e.g: @"/user/authenticate")
 @param  NSDictionary*            parameters  : the parameters of the request
 @param  NSString*                httpMethod  : the HTTP method to use for the request (default is GET)
 @param  id<HORequestDelegate> delegate    : the delegate of the request
 */
- (void)          performAsynchronousRequestForServiceWithPath:(NSString*)path 
                                                    parameters:(NSDictionary*)parameters
                                                    HTTPMethod:(NSString*)httpMethod
                                                      delegate:(id<HORequestDelegate>)delegate;

/**
 @brief  The method build a HORequest from given parameters and perform it asynchronously
 notifying the delegate from its state
 
 @param  NSString*                path        : the path of the service you wanto call (e.g: @"/user/authenticate")
 @param  NSDictionary*            parameters  : the parameters of the request
 @param  NSString*                httpMethod  : the HTTP method to use for the request (default is GET)
 @param  NSString*                identifier  : the request identifier (e.g: @"download_playlist")
 @param  id<HORequestDelegate> delegate       : the delegate of the request
 */
- (void)          performAsynchronousRequestForServiceWithPath:(NSString*)path 
                                                    parameters:(NSDictionary*)parameters
                                                    HTTPMethod:(NSString*)httpMethod
                                                    identifier:(NSString*)requestIdentifier
                                                      delegate:(id<HORequestDelegate>)delegate;


/**
 @brief  The method build a HORequest from given parameters and perform it asynchronously
 notifying the delegate from its state
 
 @param  NSString*                path        : the path of the service you wanto call (e.g: @"/user/authenticate")
 @param  NSDictionary*            parameters  : the parameters of the request
 @param  NSString*                httpMethod  : the HTTP method to use for the request (default is GET)
 @param  NSString*                identifier  : the request identifier (e.g: @"download_playlist")
 @param  id<HORequestDelegate> delegate       : the delegate of the request
 */
- (void)          performAsynchronousRequestForServiceWithPath:(NSString*)path 
                                                    parameters:(NSDictionary*)parameters
                                                    HTTPMethod:(NSString*)httpMethod
                                                    identifier:(NSString*)requestIdentifier
                                                      delegate:(id<HORequestDelegate>)delegate;

/**
 @brief  The method build a HORequest from given parameters and perform it synchronously
 
 @param  NSString*                path          : the path of the service you wanto call (e.g: @"/user/authenticate")
 @param  NSDictionary*            parameters    : the parameters of the request
 @param  NSString*                httpMethod    : the HTTP method to use for the request (default is GET)

 @return id                                     : an object depending on the response type you expect (depending on the response MIME Type).
                                                  for exemple if the type is HOResponseTypeJSON or HOResponseTypePlist
                                                  the returned object will be an NSDictionary but if it is 
                                                  HOResponseTypeBinary the returned object will be this time, an NSData.
*/
- (id)            performSynchronousRequestForServiceWithPath:(NSString*)path 
                                                   parameters:(NSDictionary*)parameters
                                                   HTTPMethod:(NSString*)httpMethod;

@end
