/*******************************************************************************
 *  @file		HOHTTPContext.m
 *  @brief		HelliOS 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		6/23/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HOHTTPContext.h"
#import "HORequestQueue.h"

NSString  *HOHTTPContextDidLoadNotification            = @"HOResponseDidLoadNotification";
NSString  *HOHTTPContextDidFailNotification            = @"HOResponseDidFailNotification";
NSString  *HOHTTPContextDidCancelNotification          = @"HOResponseDidCancelNotification";
NSString  *HOHTTPContextDidReceiveResponseNotification = @"HOResponseDidReceiveResponseNotification";

@implementation HOHTTPContext

@synthesize request  = _request, 
            response = _response,
            payload  = _payload,
            loading  = _loading;

#pragma mark -
#pragma mark Initialization

- (id)   initWithRequest:(HORequest*)request
{
    self = [super init];
    
    if (self) 
    {
        _payload         = nil;
        _request         = [request retain];
        _request.context = self;
        _response        = nil;
                
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(reachabilityChanged:)
                                                     name:kReachabilityChangedNotification 
                                                   object:nil];
        
        [[Reachability reachabilityForInternetConnection] startNotifier];
    }
        
    return self;
}

+ (id)   HTTPContextWithRequest:(HORequest*)request
{
    return [[[self alloc] initWithRequest:request] autorelease];
}

#pragma mark -
#pragma mark Memory management

- (void) dealloc
{
    _request.context = nil;
    /**<<note: keep the request in the queue.*/
//    [[HORequestQueue sharedQueue] removeRequest:_request]; 
    HO_SAFE_RELEASE(_request);
    _response.context = nil;
    HO_SAFE_RELEASE(_response);
    HO_SAFE_RELEASE(_payload);
    HO_SAFE_RELEASE(_connection);
    [super dealloc];
}

- (void) loadAsynchronously
{
    [_payload release];
    _payload    = [NSMutableData new];
    _connection = [[NSURLConnection alloc] initWithRequest:_request delegate:self];
    
    if (_connection) 
    {
        _loading = YES;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
}

- (id) loadSynchronously
{
    NSHTTPURLResponse   *HTTPResponse   = nil;
    NSError             *error          = nil;

    _loading                            = YES;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [_payload release];
    _payload                            = [[NSURLConnection sendSynchronousRequest:_request returningResponse:&HTTPResponse error:&error] mutableCopy];
    _loading                            = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    _response                           = [[HOResponse alloc] initWithURL:[HTTPResponse URL] 
                                                                 MIMEType:[HTTPResponse MIMEType]
                                                    expectedContentLength:(NSInteger)[HTTPResponse expectedContentLength]
                                                         textEncodingName:[HTTPResponse textEncodingName]
                                                                   status:[HTTPResponse statusCode]
                                                                  context:self];    
    _response.error                     = error;
            
	if (_response.error != nil) 
    {
        NSLog(@"[HORequest] Error for request : %@ : %@ ", self, [_response.error localizedDescription]);
    }
    
    switch ([_response type]) 
    {
        case HOResponseTypeJSON:
            return [HODataConverter JSONFromData:_payload];
            break;
        case HOResponseTypePlist:
            return [HODataConverter plistFromData:_payload];
            break;
        default:
            return _payload;
            break;
    }
}

- (void) cancel
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [_payload release], _payload = nil;
    _loading = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:HOHTTPContextDidCancelNotification object:self];
    if (_connection) 
    {
        [_connection cancel],     
        HO_SAFE_RELEASE(_connection);
    }
}

#pragma mark --
#pragma mark NSURLConnection delegate methods

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response 
{        
    _response = [[HOResponse alloc] initWithURL:[response URL] 
                                       MIMEType:[response MIMEType]
                          expectedContentLength:(NSInteger)[response expectedContentLength]
                               textEncodingName:[response textEncodingName]
                                         status:[response statusCode]
                                        context:self];

    if ([_request.delegate respondsToSelector:@selector(request:didReceiveResponse:)])
        [_request.delegate request:_request didReceiveResponse:_response];
    [[NSNotificationCenter defaultCenter] postNotificationName:HOHTTPContextDidReceiveResponseNotification object:self];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{	  
	[_payload appendData:data];
    
    if ([_request.delegate respondsToSelector:@selector(request:didReceiveData:)]) 
    {
        [_request.delegate request:_request didReceiveData:data];
    }	
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{   
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

//    _response.error = error;
	
    if ([_request.delegate respondsToSelector:@selector(request:didFailWithError:)]) 
    {
		[_request.delegate request:_request didFailWithError:error];
	}
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:HOHTTPContextDidFailNotification object:self];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection 
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    switch ([_response type]) 
    {
        case HOResponseTypeJSON:
            if ([_request.delegate respondsToSelector:@selector(request:didSuccessWithObject:)]) {
                [_request.delegate request:_request didSuccessWithObject:[HODataConverter JSONFromData:_payload]];
            }
            
            break;
        case HOResponseTypePlist:
            if ([_request.delegate respondsToSelector:@selector(request:didSuccessWithObject:)]) {
                [_request.delegate request:_request didSuccessWithObject:[HODataConverter plistFromData:_payload]];
            }
            
            break;
            
        default:
            if ([_request.delegate respondsToSelector:@selector(request:didSuccessWithData:)]) {
                [_request.delegate request:_request didSuccessWithData:_payload];
            }
            
            break;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:HOHTTPContextDidLoadNotification object:self];    
}

#pragma mark - 
#pragma mark Reachability status listener

- (void) reachabilityChanged:(NSNotification* )notification
{
    Reachability *currentReachability = [notification object];
    
    if ([currentReachability currentReachabilityStatus] == NotReachable) 
    {
        [self cancel];
    }
    else if (!_connection)
    {
        [self loadAsynchronously];
    }
}
    
@end
