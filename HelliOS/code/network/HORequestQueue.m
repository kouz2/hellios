/*******************************************************************************
 *  @file		HORequestQueue.m
 *  @brief		HelliOS 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		5/30/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HORequestQueue.h"

@interface HORequestQueue (Private)
- (void) _fireNextRequest;
@end

static HORequestQueue* sharedQueue = nil;

@implementation HORequestQueue

@synthesize requests = _requests;

#pragma mark -
#pragma mark Singleton

+ (HORequestQueue *)sharedQueue
{
	@synchronized(self)
	{
		if (sharedQueue == nil)
			sharedQueue = [[self alloc] init];
	}
	return sharedQueue;
}

+ (id)allocWithZone:(NSZone *)zone
{
	@synchronized(self)
	{
		if (sharedQueue == nil)
		{
			sharedQueue = [super allocWithZone:zone];
			return sharedQueue;
		}
	}
   
	return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

- (id)retain
{
	return self;
}

- (NSUInteger)retainCount
{
	return NSUIntegerMax;
}

- (void)release
{
}

- (id)autorelease
{
	return self;
}


- (id) init
{
    self = [super init];
    
    if (self)
    {
        _loading = NO;
        _requests = [NSMutableArray new];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(HTTPContextDidLoadNotification:) 
                                                     name:HOHTTPContextDidLoadNotification 
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(HTTPContextDidFailNotification:) 
                                                     name:HOHTTPContextDidFailNotification 
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(HTTPContextDidCancelNotification:) 
                                                     name:HOHTTPContextDidCancelNotification
                                                   object:nil];
    }
    return self;
}

#pragma mark -
#pragma mark request responses

- (void) HTTPContextDidLoadNotification:(NSNotification*)notification
{
    _loading = NO;
    [_requests removeObject:((HOHTTPContext*)notification.object).request];
    [self _fireNextRequest];  
}

- (void) HTTPContextDidCancelNotification:(NSNotification*)notification
{
    [self HTTPContextDidLoadNotification:notification];/**<@note requires the same action (remove) */
}

- (void) HTTPContextDidFailNotification:(NSNotification*)notification
{
    _loading = NO;
    /*<@note:permutation of requests*/
    HORequest *recievedRequest = ((HOHTTPContext*)notification.object).request;
    NSUInteger index1 = [_requests indexOfObject:recievedRequest];
    [_requests addObject:recievedRequest];
    if(index1 != NSNotFound)
        [_requests removeObjectAtIndex:index1];


    [self _fireNextRequest];
}

#pragma mark -
#pragma mark Public

- (void) addRequest:(HORequest*)request
{    
    [_requests addObject:request];
    if (_loading == NO || [_requests count] == 1) 
    {
        [self _fireNextRequest];
    }
}

- (void) removeRequest:(HORequest*)request;
{
    if (![_requests containsObject:request])
        return;
    if (request.context != nil && request.context.loading)
        [request.context cancel];
    [_requests removeObject:request];
    [self _fireNextRequest];
}

#pragma mark -
#pragma mark Private

- (void) _fireNextRequest
{    
    if (_requests == nil || [_requests count] < 1) 
        return;
    _loading = YES;
    [[HOHTTPContext HTTPContextWithRequest:[_requests objectAtIndex:0]] loadAsynchronously];    
}

@end
