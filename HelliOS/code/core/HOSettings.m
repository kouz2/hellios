//
//  HOSettings.m
//  HelliOS
//
//  Created by Olivier THIERRY on 4/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HOSettings.h"

NSString* const kHOSettingsApplicationName                    = @"kHOSettingsApplicationName";
NSString* const kHOSettingsiTunesApplicationID                = @"kHOSettingsiTunesApplicationID";
NSString* const kHOSettingsSupportedInterfaceOrientations	  = @"kHOSettingsSupportedInterfaceOrientations";
NSString* const kHOSettingsNotificationsURL                   = @"kHOSettingsNotificationsURL";

@implementation HOSettings

@synthesize applicationName				   = _applicationName,
            iTunesApplicationID			   = _iTunesApplicationID,
			supportedInterfaceOrientations = _supportedInterfaceOrientations,
            notificationsURL               = _notificationsURL,
            providers                      = _providers;

#pragma mark --
#pragma mark memory management

-(void) dealloc 
{
    HO_SAFE_RELEASE(_applicationName);
    HO_SAFE_RELEASE(_iTunesApplicationID);
    HO_SAFE_RELEASE(_supportedInterfaceOrientations);
    HO_SAFE_RELEASE(_notificationsURL);
    HO_SAFE_RELEASE(_providers);
    [super dealloc];
}

- (void) load:(NSDictionary *)settings {
	self.applicationName				= [settings objectForKey:kHOSettingsApplicationName];
    self.iTunesApplicationID			= [settings objectForKey:kHOSettingsiTunesApplicationID];
    self.supportedInterfaceOrientations	= [settings objectForKey:kHOSettingsSupportedInterfaceOrientations];
    self.notificationsURL               = [settings objectForKey:kHOSettingsNotificationsURL];
}

@end
