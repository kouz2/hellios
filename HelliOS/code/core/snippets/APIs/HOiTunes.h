/**
 @file		HOiTunes.h
 @brief		HOiTunes 
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.

 This class provides methods to communicate with iTunes search API.
 You can either query the iTunes database to fetch product info or
 directly launch the itunes store with a given query.
*/

#import "HOEncoder.h"

@interface HOiTunes : NSObject {}

/**
 @brief  This method query the iTunes API and return the result
 
 @param  NSString*     query       : the query (e.g: @"Michael jackson - Bad")
 
 @return NSDictionary*             : the result of the query provided by iTunes API 
*/
+ (NSDictionary*) getResultsFromItunesWithQuery:(NSString*)query;

/**
 @brief  This method query the iTunes API and return the URL of the FIRST product it has found
         if, of course, there is one. Otherwhise, this method will return nil
 
 @param  NSString*     query       : the query (e.g: @"Michael jackson - Bad")
 
 @return NSURL*                    : the fetched product URL
*/
+ (NSURL*)        getProductURLFromItunesWithQuery:(NSString*)query;

/**
 @brief  This method query the iTunes API and use the URL of the FIRST product it has found
         to open iTunes application on this product page.
 
 @param  NSString*     query       : the query (e.g: @"Michael jackson - Bad")
*/
+ (void)          launchItunesWithQuery:(NSString*)query;

@end
