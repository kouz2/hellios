/**
 @file		HOEncoder.h
 @brief		HOEncoder 
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.
 
 Encoding methods
 */

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface HOEncoder : NSObject

/**
 @brief  this method build an MD5 from a given string model
 
 @param  NSString* inputStr : the string to convert into MD5

 @return NSString*          : MD5 string 
*/
+ (NSString *) encodeStringToMD5:(NSString*)inputStr;


/**
 @brief this method encode then build an URL
 
 @param  NSString* url      : the string url to encode
 
 @return NSURL*             : the encoded URL as an NSURL object 
*/
+ (NSURL*)     encodeURL:(NSString*)url;

@end
