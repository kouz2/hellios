/*******************************************************************************
 *  @file		HOAppStoreLink.h
 *  @brief		HelliOS 
 *  @author		Sergio Kunats
 *  @version	1.0
 *  @date		7/25/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *******************************************************************************/

@interface HOAppStoreLink : NSObject {
    
}

+ (NSURL*) viewContentsUserReviews:(NSString*)appId;
+ (NSURL*) viewSoftware:(NSString*)appId;
+ (NSURL*) viewAlbum:(NSString*)albumId;
+ (NSURL*) viewArtist:(NSString*)artistId;
+ (NSURL*) viewPodcast:(NSString*)podcastId;
+ (NSURL*) addUserReview:(NSString*)appId;

@end
