//
//  ErrorHandler.m
//  Roll and Jump
//
//  Created by Sergio Kunats on 6/9/11.
//  Copyright 2011 www.chugulu.com. All rights reserved.
//

#import "HOErrorHandler.h"
#import <UIKit/UIKit.h>

@interface HOErrorHandler (Private)

+ (void) showAlertViewOfDeath;
+ (void) localReport:(const char *)selector inFile:(const char*)fileName onLine:(const int)lineNumber error:(NSString *)typeOrName details:(id)details fatal:(BOOL)isFatal;
+ (void) remoteReport:(const char *)selector inFile:(const char*)fileName onLine:(const int)lineNumber error:(NSString *)typeOrName details:(id)details fatal:(BOOL)isFatal;
+ (void) remoteReportType:(NSString *)type extras:(NSDictionary *)dico;

@end

@implementation HOErrorHandler

+ (void) reportErrorSelector:(const char *)selector inFile:(const char*)fileName onLine:(const int)lineNumber error:(NSString *)typeOrName details:(id)details fatal:(BOOL)isFatal {
    [[self class] localReport:selector inFile:fileName onLine:lineNumber error:typeOrName details:details fatal:isFatal];
    [[self class] remoteReport:selector inFile:fileName onLine:lineNumber error:typeOrName details:details fatal:isFatal];
    if (isFatal)
        [[self class] showAlertViewOfDeath];
}

+ (void) localReport:(const char *)selector inFile:(const char *)fileName onLine:(const int)lineNumber error:(NSString *)typeOrName details:(id)details fatal:(BOOL)isFatal {
    NSLog(@"##### %@ERROR [%@] #####\n[%s:%d]\n%s\nDETAILS:\n%@\n##### %@ERROR END [%@] #####", (isFatal ? @"FATAL ": @""), typeOrName, fileName, lineNumber, selector, details, (isFatal ? @"FATAL ": @""), typeOrName);
}

// override this method to report to your analytics service (Flurry, Capptain...)
+ (void) remoteReportType:(NSString *)type extras:(NSDictionary *)dico {
//    [FlurryAnalytics logError:typeOrName message:errorMessage error:nil];
//    [[CapptainAPI sharedAPI] sendError:typeOrName extras:extra];
}

+ (void) remoteReport:(const char *)selector inFile:(const char *)fileName onLine:(const int)lineNumber error:(NSString *)typeOrName details:(id)details fatal:(BOOL)isFatal {
    NSMutableDictionary *extra = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                          [NSString stringWithUTF8String:fileName], @"fileName",
                                          [NSNumber numberWithUnsignedInt:lineNumber], @"lineNumber",
                                          [NSString stringWithUTF8String:selector], @"selector",
                                          (isFatal ? @"YES": @"NO"), @"fatal",
                                          nil];
    if ([details isKindOfClass:[NSString class]])
        [extra setValue:details forKey:@"description"];
    else if ([details isKindOfClass:[NSDictionary class]])
        [extra setValuesForKeysWithDictionary:details];
    else if ([details isKindOfClass:[NSError class]]) {
        [extra setValue:[NSNumber numberWithInteger:[details code]] forKey:@"code"];
        [extra setValue:[details domain] forKey:@"domain"];
        [extra setValue:[details localizedDescription] forKey:@"localizedDescription"];
        [extra setValue:[details localizedRecoveryOptions] forKey:@"localizedRecoveryOptions"];
        [extra setValue:[details localizedRecoverySuggestion] forKey:@"localizedRecoverySuggestion"];
        [extra setValue:[details localizedFailureReason] forKey:@"localizedFailureReason"];
        [extra setValue:[details userInfo] forKey:@"useInfo"];
    }
    NSMutableString* errorMessage = [[NSMutableString alloc] init];
    for (NSString* key in [extra allKeys])
        [errorMessage appendFormat:@"[%@]\n%@\n\n", key, [extra objectForKey:key]];
    
    [[self class] remoteReportType:typeOrName extras:extra];
    [extra release];
    [errorMessage release];
}

+ (void) showAlertViewOfDeath {
    UIAlertView *alertViewOfDeath = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"\n\n%@", NSLocalizedString(@"HO_ERROR_HANDLER_FATAL", nil)]
                                                               message:nil
                                                              delegate:nil
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:nil];
    [alertViewOfDeath show];
    [alertViewOfDeath release];
}

+ (void) showWarningAlert:(NSString*)warning {
    if(warning == nil || [warning isKindOfClass:[NSNull class]])
        return;
    UIAlertView *alertViewOfDeath = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"HO_ERROR_HANDLER_WARNING", nil)
                                                               message:NSLocalizedString(warning, nil)
                                                              delegate:nil
                                                     cancelButtonTitle:NSLocalizedString(@"HO_ERROR_HANDLER_WARNING_DISMISS", nil)
                                                     otherButtonTitles:nil];
    [alertViewOfDeath show];
    [alertViewOfDeath release];
}

@end
