/**
 @file		HOAppRating.h
 @brief		HelliOS App Store App Rating prompt
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.

 This HelliOS extention provides methods to ask your users to rate your app
 with a single line of code.
 You can provide your custom text to display in the proposal (UIAlertView)
 
 @note Please make sure you have correctly initialized HelliOS by providing
       your application itunes ID and name in the settings (kHOSettingsiTunesApplicationIDKey, kHOSettingsApplicationNameKey)
 
 @note You have to add following translations to your locales files:
     "HORATING_TITLE_FORMAT"     = "Rate %@";
     "HORATING_YES"              = "Yes";
     "HORATING_NOT_NOW"          = "Not now";
     "HORATING_NEVER_ASK"        = "Never ask me again";
     "HORATING_PLEASE_RATE_ME"   = "Please rate this awesome game";
*/

typedef enum {
    HOAppRatingViewButtonCancel     = 0,
    HOAppRatingViewButtonRate       = 1,
    HOAppRatingViewButtonNeverAsk   = 2
} HOAppRatingViewButton;

#define isValidRatingButton(buttonIndex) (buttonIndex == HOAppRatingViewButtonCancel || buttonIndex == HOAppRatingViewButtonRate || buttonIndex == HOAppRatingViewButtonNeverAsk)

@protocol HOAppRatingDelegate <NSObject,UIAlertViewDelegate>

- (void) checkUserShouldRateApplication;

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@optional
- (BOOL) checkUserShouldRateApplicationAndShow;

@end

@interface HOAppRating : NSObject {

}

/**
 @brief  This method display an alert to the user and ask him to rate you application
         on the app store.
  
 @note   Please make sure you have correctly initialized HelliOS by providing
         your application itunes ID and name in the settings (kHOSettingsiTunesApplicationIDKey, kHOSettingsApplicationNameKey)
*/
+ (void) showApplicationRatingProposal:(id<HOAppRatingDelegate>)ratingDelegate askNeverAsk:(BOOL)neverAsk;


/**
 @brief  This method display an alert with a custom text to the user and ask him to 
         rate you application on the app store.
 
 @param  NSString*     text       : the custom text to display in the alert

 @note   Please make sure you have correctly initialized HelliOS by providing
         your application itunes ID and name in the settings (kHOSettingsiTunesApplicationIDKey, kHOSettingsApplicationNameKey) 
*/
+ (void) showApplicationRatingProposal:(id<HOAppRatingDelegate>)ratingDelegate askNeverAsk:(BOOL)neverAsk withCustomText:(NSString*)text;

/**
 @brief This method opens AppStore on the User Reviews page
 */
+ (BOOL) openAppStoreRatingPage;

@end
