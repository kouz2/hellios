//
//  ErrorHandler.h
//  Roll and Jump
//
//  Created by Sergio Kunats on 6/9/11.
//  Copyright 2011 www.chugulu.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#define reportError(_nameOrType_, _details_)        reportErrorSelector:__FUNCTION__ \
                                                    inFile:__FILE__ \
                                                    onLine:__LINE__ \
                                                    error:_nameOrType_ \
                                                    details:_details_ \
                                                    fatal:NO

#define reportFatalError(_nameOrType_, _details_)   reportErrorSelector:__FUNCTION__ \
                                                    inFile:__FILE__ \
                                                    onLine:__LINE__ \
                                                    error:_nameOrType_ \
                                                    details:_details_ \
                                                    fatal:YES

@interface HOErrorHandler : NSObject {
    
}

+ (void) reportErrorSelector:(const char*)selector inFile:(const char*)fileName onLine:(const int)lineNumber error:(NSString*)typeOrName details:(id)details fatal:(BOOL)isFatal;
+ (void) showWarningAlert:(NSString*)warning;

@end
