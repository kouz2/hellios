/*******************************************************************************
 *  @file		HOAppStoreLink.m
 *  @brief		HelliOS 
 *  @author		Sergio Kunats
 *  @version	1.0
 *  @date		7/25/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HOAppStoreLink.h"

@implementation HOAppStoreLink

+ (NSURL*) viewContentsUserReviews:(NSString*)appId {
    return [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", appId]];
}

+ (NSURL*) viewSoftware:(NSString*)appId {
    return [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?type=Purple+Software&id=%@", appId]];
}

+ (NSURL*) viewAlbum:(NSString*)albumId {
    return [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewAlbum?type=Purple+Software&id=%@", albumId]];
}

+ (NSURL*) viewArtist:(NSString*)artistId {
    return [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewArtist?type=Purple+Software&id=%@", artistId]];
}

+ (NSURL*) viewPodcast:(NSString*)podcastId {
    return [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewPodcast?type=Purple+Software&id=%@", podcastId]];
}

+ (NSURL*) addUserReview:(NSString*)appId {
    return [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://userpub.itunes.apple.com/WebObjects/MZUserPublishing.woa/wa/addUserReview?type=Purple+Software&id=%@", appId]];
}

@end
