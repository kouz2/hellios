//
//  HODelegate.m
//  HelliOS
//
//  Created by Olivier THIERRY on 5/3/11.
//  Copyright 2011 Chugulu. All rights reserved.
//

#import "HODelegate.h"


@implementation HODelegate

@synthesize target   = _target,
			object   = _object,
		    selector = _selector;

#pragma mark --
#pragma mark Initialization

+ (HODelegate*) delegateWithTarget:(id)target selector:(SEL)selector
{
	return [HODelegate delegateWithTarget:target selector:selector object:nil];
}

+ (HODelegate*) delegateWithTarget:(id)target selector:(SEL)selector object:(id)object
{
	HODelegate *delegate = [[[HODelegate alloc] init] autorelease];
	delegate.target		 = target;
	delegate.selector    = selector;
	delegate.object		 = object;
	
	return delegate;
}

#pragma mark --
#pragma mark Callback performer method

- (void) perform 
{
	if (_object) 
    {
		[_target performSelector:_selector withObject:_object];
	}
	else 
    {
		[_target performSelector:_selector];
	}
}

#pragma mark --
#pragma mark Memory managment

- (void)dealloc 
{
    HO_SAFE_RELEASE(_target);
    HO_SAFE_RELEASE(_object);
    [super dealloc];
}


@end
