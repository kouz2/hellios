/**
 @class		HOSplashViewController
 @author	Olivier Thierry
 @version	1.3
 @date		03/02/2011
 @note		This view controller is used to display a splash screen animation
			from a given images array.
*/

#define BUNDLE_VERSION_LABEL_TAG	1

#define IMAGE_DISPLAY_DURATION		2

@class HOSplashViewController;

@protocol HOSplashViewControllerDelegate <NSObject>

- (void)splashViewControllerAnimationDidFinish:(HOSplashViewController*)splashViewController;

@end

@protocol HOSplashContentSequence <NSObject>

- (BOOL) contentSequenceDidFinish;

@end

@class HOSplash;

@interface HOSplashViewController : UIViewController {
	
	id<HOSplashViewControllerDelegate>		delegate;
	
	BOOL									shouldDisplayBundleVersion;
	
    NSUInteger                              splashOrientation;
@private
	NSArray									*_splashes;				/**< Array of UIViewController/UIView/UIImage objects (and subclasses) and/or strings (image file names) **/
    NSTimeInterval                          *_timeIntervals;
    NSInteger                               _currentSplashIndex;
    id                                      _observedSplash;
    UIView                                  *_previousView;
    BOOL                                    _firstSplash;
    NSTimer*                                _timer;
    NSTimeInterval                          _timeDelta;
    HOSplash                                *_currentSplash;
}

@property (nonatomic, retain)	id<HOSplashViewControllerDelegate>		delegate;
@property (nonatomic, assign)	BOOL									shouldDisplayBundleVersion;
@property (nonatomic, assign)   NSUInteger                              splashOrientation;

// initialization
- (id) initWithSplashImages:(NSArray*)images;
- (id) initWithSplashImages:(NSArray*)images timeIntervals:(NSTimeInterval*)timeIntervals;

@end
