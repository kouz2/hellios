//
//  HOSplash.h
//  HelliOS
//
//  Created by Sergio Kunats on 3/26/12.
//  Copyright (c) 2012 Chugulu. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const HOSplashUntimedSplashEndSelectorName;

@interface HOSplash : NSObject 

@property (nonatomic, retain) id splash;
@property (nonatomic, retain) UIView* splashView;
@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, readonly, getter = isObservable) BOOL observable;
@property (nonatomic, readonly, getter = isFinished) BOOL finished;

- (id) initWithSplash:(id)someSplash view:(UIView *)someSplashView duration:(NSTimeInterval)someDuration;

@end
