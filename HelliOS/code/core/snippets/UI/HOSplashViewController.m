//
//  HOSplashViewController.m
//  blind_test
//
//  Created by Olivier THIERRY on 2/3/11.
//  Copyright 2011  . All rights reserved.
//

#import "HOSplash.h"
#import "HOSplashViewController.h"

const NSTimeInterval animationDuration = 0.5;

@implementation HOSplashViewController

@synthesize shouldDisplayBundleVersion, delegate, splashOrientation;

- (id) initWithSplashImages:(NSArray*)images timeIntervals:(NSTimeInterval*)timeIntervals {
	if ((self = [super init])) {
        splashOrientation   = UIInterfaceOrientationPortrait;
		_splashes           = [images retain];
        _currentSplashIndex = -1;
        _timeIntervals      = timeIntervals;
        _timer              = nil;
        _timeDelta          = 0;
        _observedSplash     = nil;
        _firstSplash        = YES;
        _previousView       = nil;
        _currentSplash      = nil;
	}
	return self;
}

- (id) initWithSplashImages:(NSArray*)images {
    return [self initWithSplashImages:images timeIntervals:NULL];
}

- (void) resetObservedSplash {
    if (_observedSplash) {
        [_observedSplash removeObserver:self forKeyPath:HOSplashUntimedSplashEndSelectorName];
        _observedSplash = nil;
    }
}

- (void) splashesDidEnd {
    if (delegate && [delegate respondsToSelector:@selector(splashViewControllerAnimationDidFinish:)])
        [delegate splashViewControllerAnimationDidFinish:self];
    [_timer invalidate], _timer = nil;
}

- (UIView *) newSplashViewWithSplashObject:(id)splash {
	UIView *splashView  = nil;
    if ([splash isKindOfClass:[UIView class]])
        splashView      = [splash retain];
    else if ([splash isKindOfClass:[UIViewController class]])
        splashView      = [((UIViewController*)splash).view retain];
    else if ([splash isKindOfClass:[UIImage class]])
        splashView      = [[UIImageView alloc] initWithImage:splash];
    else if ([splash isKindOfClass:[NSString class]])
        splashView      = [[UIImageView alloc] initWithImage:[UIImage imageNamed:splash]];
    return splashView;
}

- (HOSplash *) newSplashScreen {
    HOSplash *splashScreen  = nil;
    NSInteger max           = [_splashes count];
    UIView* splashView      = nil;
    id splash               = nil;
    BOOL observable         = NO;
    NSTimeInterval duration = IMAGE_DISPLAY_DURATION;
    while (splashView == nil && _currentSplashIndex + 1 < max) {
        splash              = [_splashes objectAtIndex:++_currentSplashIndex];
        observable          = [splash respondsToSelector:NSSelectorFromString(HOSplashUntimedSplashEndSelectorName)];
        if (!observable || ![splash contentSequenceDidFinish])
            splashView = [self newSplashViewWithSplashObject:splash];
        if (!observable && _timeIntervals != NULL && _timeIntervals[_currentSplashIndex] > 0.0)
            duration = _timeIntervals[_currentSplashIndex];
    }
    if (splashView) {
        splashScreen = [[HOSplash alloc] initWithSplash:splash view:splashView duration:duration];
        [splashView release];
    }
    return splashScreen;
}

- (BOOL) isFirstSplash {
    return _currentSplashIndex <= 0;
}

- (void) scheduleNextSplash {
    SEL timeEndSelector = @selector(showNextSplash);
    if (_currentSplashIndex == [_splashes count] - 1) // last splash
        timeEndSelector = @selector(splashesDidEnd);

    if (![_currentSplash isObservable]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseTimer) name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeTimer) name:UIApplicationDidBecomeActiveNotification object:nil];
        _timer = [NSTimer scheduledTimerWithTimeInterval:_currentSplash.duration target:self selector:timeEndSelector userInfo:nil repeats:NO];
    }
    else {
        if ([_currentSplash isFinished])
            [self performSelector:timeEndSelector];
        else {
            [_currentSplash.splash addObserver:self forKeyPath:HOSplashUntimedSplashEndSelectorName options:NSKeyValueObservingOptionNew context:NULL];
            _observedSplash = _currentSplash.splash;
        }
    }
}

- (void) showNextSplash {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [_timer invalidate], _timer = nil;
    _timeDelta = 0;

    __block HOSplash* splash = [self newSplashScreen];
    if (splash == nil)
        return [self splashesDidEnd];

    if (UIInterfaceOrientationIsPortrait(splashOrientation))
        splash.splashView.frame			= CGRectMake(0, 0, 320, 480);
    else
        splash.splashView.frame			= CGRectMake(0, 0, 480, 320);
    splash.splashView.autoresizingMask  = UIViewAutoresizingNone;
	splash.splashView.alpha				= [self isFirstSplash] ? 1.0f : 0.0f;

	[self.view addSubview:splash.splashView];    

    void (^changeSplash)(BOOL) = ^(BOOL finished) {
        if (!finished && splash.splashView.alpha < 1.0f) // sometimes alpha = 1.0 even if the animation is not finished (ex: background/resume)
            return;
        if (_currentSplash) {
            [_currentSplash.splashView removeFromSuperview];
            [_currentSplash release];
        }
        _currentSplash = splash;

        [self scheduleNextSplash];
    };
    
    if ([self isFirstSplash])
        changeSplash(YES);
    else {
        [UIView animateWithDuration:animationDuration
                         animations:^{
                             splash.splashView.alpha = 1.0f;
                             if (self.shouldDisplayBundleVersion) {
                                 [self.view bringSubviewToFront:[self.view viewWithTag:BUNDLE_VERSION_LABEL_TAG]];
                                 [self.view viewWithTag:BUNDLE_VERSION_LABEL_TAG].alpha = 1.0f;
                             }
                         }
                         completion:changeSplash];
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (![keyPath isEqualToString:HOSplashUntimedSplashEndSelectorName])
        return ;
    id newValue = [change objectForKey:NSKeyValueChangeNewKey];
    if (newValue == nil || ![newValue isKindOfClass:[NSNumber class]] || [newValue boolValue] != YES)
          return ;
    [self resetObservedSplash];
    [self showNextSplash];
}

- (void)viewDidLoad {
    [super viewDidLoad];

	if (_splashes && [_splashes count] > 0)
		[self showNextSplash];

	if (self.shouldDisplayBundleVersion) {
		UILabel *versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(440, 300, 40, 20)];

		[versionLabel setText:[NSString stringWithFormat:@"v.%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]]];
		[versionLabel setBackgroundColor:[UIColor clearColor]];
		[versionLabel setTextColor:[UIColor whiteColor]];
		[versionLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:10]];
		[versionLabel setTextAlignment:UITextAlignmentCenter];
		[versionLabel setClipsToBounds:YES];
		[versionLabel setTag:BUNDLE_VERSION_LABEL_TAG];
		[versionLabel setAlpha:0.0f];

		[self.view addSubview:versionLabel];

		[versionLabel release];
	}
}

- (void)viewDidUnload {
    [self resetObservedSplash];
    [super viewDidUnload];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [self resetObservedSplash];
	[_splashes release];
    if (_timer)
        [_timer invalidate], _timer = nil;
    if (_timeIntervals != NULL)
        free(_timeIntervals);
    [super dealloc];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation) == UIInterfaceOrientationIsPortrait(splashOrientation);
}

#pragma mark - Listeners

- (void) pauseTimer {
    _timeDelta = [_timer.fireDate timeIntervalSinceNow];
    [_timer invalidate], _timer = nil;
}

- (void) resumeTimer {
    if (_timer == nil)
        _timer = [NSTimer scheduledTimerWithTimeInterval:_timeDelta target:self selector:@selector(showNextSplash) userInfo:nil repeats:NO];
}

@end
