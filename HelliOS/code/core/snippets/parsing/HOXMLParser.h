/*******************************************************************************
 *  @file		HOXMLParser.h
 *  @brief		HOFeedFetcher 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		5/5/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 *******************************************************************************/

#import <Foundation/Foundation.h>
#import "HODelegate.h"

#import "HOXMLNode.h"

@class HOXMLParser;

@protocol HOXMLParserDelegate <NSObject>

- (void) parser:(HOXMLParser*)parser didFinishParsingWithRootObject:(NSDictionary*)object;

@end

@interface HOXMLParser : NSObject <NSXMLParserDelegate> {
        
@private
    id<HOXMLParserDelegate>      _delegate;

    NSXMLParser                 *_parser;
    HOXMLNode                   *_currentNode;
}

@property (nonatomic, assign)   id<HOXMLParserDelegate> delegate;

- (id)   initWithData:(NSData*)data;
- (id)   initWithContentsOfURL:(NSURL*)url;

- (BOOL) parse;

@end
