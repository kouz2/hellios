/*******************************************************************************
 *  @file		HOXMLParser.m
 *  @brief		HOFeedFetcher 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		5/5/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HOXMLParser.h"

@implementation HOXMLParser

@synthesize delegate    = _delegate;

#pragma mark --
#pragma mark Initialization

- (id) initWithData:(NSData*)data
{
    self = [super init];
    
    if (self) {
        _parser = [[NSXMLParser alloc] initWithData:data];
        [_parser setDelegate:self];
    }
    
    return self;    
}

- (id) initWithContentsOfURL:(NSURL*)url
{
    self = [super init];
    
    if (self) {
        _parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        [_parser setDelegate:self];
    }
    
    return self;
}

#pragma mark --
#pragma mark Public method

- (BOOL) parse
{
    return [_parser parse];
}

#pragma mark --
#pragma mark NSXMLParserDelegate methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    HOXMLNode *node   = [[[HOXMLNode alloc] initWithName:elementName] autorelease];
    node.parent       = _currentNode;
    
    _currentNode      = node;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    // check if the current note has a parent, otherwise it's the root node
    if (_currentNode.parent) 
    {
        // check if the current node is a objects container or a string container
        // e.g : 
        // <post>
        //      <title>toto</title>
        //      <content>toto</content>
        // </post>
        // 
        // Here, <post> is an objects container whereas <title> or <content> are string containers
        if ([_currentNode.objects count]) 
        {
            if ([_currentNode.parent.objects objectForKey:_currentNode.name]) 
            {
                id object = [_currentNode.parent.objects objectForKey:_currentNode.name];
                
                if ([object isKindOfClass:[NSMutableArray class]]) {
                    [(NSMutableArray*)object addObject:_currentNode.objects];
                }
                else {                    
                    [_currentNode.parent.objects setObject:[NSMutableArray arrayWithObjects:[_currentNode.parent.objects objectForKey:_currentNode.name], _currentNode.objects, nil] forKey:_currentNode.name];
                }
                
            }
            else
            {
                [_currentNode.parent.objects setObject:_currentNode.objects forKey:_currentNode.name];                                        
            }
        }
        else 
        {
            [_currentNode.parent.objects setObject:_currentNode.content forKey:_currentNode.name];            
        }
        
        _currentNode = _currentNode.parent;
        
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    _currentNode.content = string;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser 
{
    if ([_delegate respondsToSelector:@selector(parser:didFinishParsingWithRootObject:)])
    {
        [_delegate parser:self didFinishParsingWithRootObject:_currentNode.objects];
    }
}

#pragma mark --
#pragma mark Memory managment

- (void) dealloc
{
    [_parser release];     _parser = nil;
    [super dealloc];
}

@end
