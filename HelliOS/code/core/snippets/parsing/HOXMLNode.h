/*******************************************************************************
 *  @file		HOXMLNode.h
 *  @brief		HOFeedFetcher 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		5/5/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 *******************************************************************************/

#import <Foundation/Foundation.h>


@interface HOXMLNode : NSObject {
@private
    HOXMLNode               *_parent;
    NSString                *_name;
    NSString                *_content;
    NSMutableDictionary     *_objects;
}

@property (nonatomic, assign) HOXMLNode           *parent;
@property (nonatomic, retain) NSString            *name;
@property (nonatomic, retain) NSString            *content;
@property (nonatomic, retain) NSMutableDictionary *objects;

- (id) initWithName:(NSString*)nodeName;

@end
