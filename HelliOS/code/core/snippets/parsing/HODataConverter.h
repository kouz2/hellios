/**
 @file		HOiTunes.h
 @brief		HOiTunes 
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.
 
 HODataConverter provides methods to get specific data type for a given object to parse
 You can, for example get a JSON object by giving a binary or string object.
 HODataConverter it based on external technologies for JSON parsing.
*/

#import <Foundation/Foundation.h>

@interface HODataConverter : NSObject {}

/**
 @brief  The method parse binary and build a representative JSON object
 
 @param  NSData*     data         : the binary data to parse
 
 @return id                       : the JSON representative object 
 */
+ (id)             JSONFromData:(NSData*)data;



/**
 @brief  The method parse a representative JSON Object and build its binary representation
 
 @param  id JSON                  : the representative JSON Object
 
 @return NSData*                  : the binary representation of the given JSON object
 */ 
+ (NSData*)        dataFromJSON:(id)JSON;
 


/**
 @brief  The method parse a string and build a representative JSON object
 
 @param  NSString*     string     : the string JSON representation
 
 @return id                       : the JSON representative object 
 */
+ (id)             JSONFromString:(NSString*)string;



/**
 @brief  The method parse a representative JSON Object and build its string representation
 
 @param  id JSON                  : the representative JSON Object
 
 @return NSString*                : the string JSON representation
 */
+ (NSString*)     stringFromJSON:(id)JSON;



/**
 @brief  The method parse binary and build a representative plist object (NSDictionary)
 
 @param  NSData*     data         : the binary data to parse
 
 @return id                       : the plist object 
 */
+ (id)            plistFromData:(NSData*)data;

/**
 @brief  The method parse binary and build a representative plist object
 
 @param  id          data         : the plist object
 
 @return NSData*                  : the binary representation of the given plist object
 */
+ (NSData*)       dataFromPlist:(id)plist;

@end
