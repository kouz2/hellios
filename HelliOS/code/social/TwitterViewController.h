/**
 *  @file		TwitterPopup.h
 *  @brief		HelliOS 
 *  @author		François Benaiteau
 *  @version	1.0
 *  @date		9/13/11
 *
 *  Copyright 	Chugulu Games 2009-2011. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import "SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"
#import "DDSocialDialog.h"
#import "TwitterPopupDelegate.h"

#define WIDTH_FOR_ORIENTATION (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? 480.0 : 320.0)
#define HEIGHT_FOR_ORIENTATION (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? 320.0 : 480.0)
#define WIDTH_MARGIN (WIDTH_FOR_ORIENTATION * 0.05)
#define WIDTH  (WIDTH_FOR_ORIENTATION - WIDTH_MARGIN)
#define HEIGHT (HEIGHT_FOR_ORIENTATION *3.0/4.0)
#define MARGIN 10
#define SPINNER_SIZE 20

@class TwitterAccountListViewController;

@interface TwitterViewController : UIViewController<DDSocialDialogDelegate, SA_OAuthTwitterControllerDelegate> {
    SA_OAuthTwitterEngine   *_twitter;
    NSString                *_tweetMessage;
    UIButton                *_tweetButton;
    UITextView              *_messageTextView;   
    UIActivityIndicatorView *_spinner;
    id<TwitterPopupDelegate>            _delegate;
    TwitterAccountListViewController    *_accountListView;
}

@property (nonatomic, assign) id<TwitterPopupDelegate> delegate;

- (id) initWithConsumerKey:(NSString*)twitterConsumerKey andConsumerSecret:(NSString*)twitterConsumerSecret andTweetMessage:(NSString*)message;
- (void) showDialog;

@end
