/**
 *  @file		TwitterPopup.m
 *  @brief		HelliOS 
 *  @author		François Benaiteau
 *  @version	1.0
 *  @date		9/13/11
 *
 *  Copyright 	Chugulu Games 2009-2011. All rights reserved.
 */

#import "TwitterViewController.h"
#import "TwitterAccountListViewController.h"
#import "HOErrorHandler.h"

#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>

@implementation TwitterViewController

@synthesize delegate = _delegate;

#pragma mark - Initialization

- (id) initWithConsumerKey:(NSString*)twitterConsumerKey andConsumerSecret:(NSString*)twitterConsumerSecret andTweetMessage:(NSString*)message 
{
    self =  [super init];
    if (self) {
        _twitter                = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];
        _twitter.consumerKey    = twitterConsumerKey;
        _twitter.consumerSecret = twitterConsumerSecret;
        _tweetMessage           = message;
        _accountListView        = nil;
    }
    return self;
}


#pragma mark - Memory

- (void) dealloc {
    _delegate = nil;
    [_twitter release];
    [_tweetButton release];
    [_accountListView release];
    [super dealloc];
}

#pragma mark - View lifecycle

- (void) loadView {
    _messageTextView = [[UITextView alloc] initWithFrame:CGRectMake(MARGIN, MARGIN, WIDTH - MARGIN*2 - 44, 3000)];
    [_messageTextView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.05]];
    [_messageTextView setEditable:NO];
    [_messageTextView setFont:[UIFont fontWithName:@"Helvetica Neue" size:18.0]];
    [_messageTextView setText:_tweetMessage];

    CGSize minimumSize = [[_messageTextView text] sizeWithFont:_messageTextView.font constrainedToSize:_messageTextView.frame.size lineBreakMode:UILineBreakModeWordWrap];

    int height = HEIGHT;
    if(minimumSize.height > HEIGHT - MARGIN - MARGIN*4 - 15 - 80)
        [_messageTextView setFrame:CGRectMake(_messageTextView.frame.origin.x, _messageTextView.frame.origin.y, _messageTextView.frame.size.width, HEIGHT - MARGIN - MARGIN*4 - 15 - 80)];
    else {
        //height = minimumSize.height + 175;
        [_messageTextView setFrame:CGRectMake(_messageTextView.frame.origin.x, _messageTextView.frame.origin.y, _messageTextView.frame.size.width, minimumSize.height + 15)];
        [_messageTextView setScrollEnabled:NO];
    }
    [_messageTextView setTextAlignment:UITextAlignmentCenter];   

    //button
    _tweetButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [_tweetButton setImage:[UIImage imageNamed:@"tweet_button.png"] forState:UIControlStateNormal];
    [_tweetButton setFrame:CGRectMake(0, 0, 113, 39)];
    [_tweetButton setCenter:CGPointMake(_messageTextView.frame.origin.x + _messageTextView.frame.size.width - _tweetButton.frame.size.width/2, height - 100)];
    [_tweetButton addTarget:self action:@selector(tweet:) forControlEvents:UIControlEventTouchUpInside];

    //spinner
    _spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, SPINNER_SIZE, SPINNER_SIZE)];
    [_spinner setCenter:CGPointMake(_tweetButton.frame.origin.x - SPINNER_SIZE,_tweetButton.frame.origin.y + _tweetButton.frame.size.height/2)];
    _spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    _spinner.hidden                     = YES;
    [_spinner setHidesWhenStopped:YES];

    UIImage* iconImage = [UIImage imageNamed:@"bird_16_blue.png"];
    //UIImage* closeImage = [UIImage imageNamed:@"FBDialog.bundle/images/close.png"];
    DDSocialDialog *twitterView = [[DDSocialDialog alloc] initWithFrame:CGRectMake(0., 0., WIDTH, height) andIcone:iconImage];
    twitterView.titleLabel.text = NSLocalizedString(@"HO_TWITTER_TITLE", nil);
    twitterView.dialogDelegate = self;

    [twitterView.contentView addSubview:_messageTextView];
    [twitterView.contentView addSubview:_tweetButton];
    [twitterView.contentView addSubview:_spinner];

    self.view = twitterView;
    self.view.autoresizesSubviews = YES;
    [twitterView release];    
}

#pragma mark - Twitter
- (void) sendTweetWithNativeFramework {
	ACAccountStore *accountStore = [[[ACAccountStore alloc] init] autorelease];
	
	// Create an account type that ensures Twitter accounts are retrieved.
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
	
	// Request access from the user to use their Twitter accounts.
    [accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error) {
        if (granted) {
            void (^tweetBlock)(ACAccount *) = ^(ACAccount *twitterAccount) {
				TWRequest *postRequest = [[TWRequest alloc] initWithURL:[NSURL URLWithString:@"http://api.twitter.com/1/statuses/update.json"]
                                                             parameters:[NSDictionary dictionaryWithObject:_tweetMessage
                                                                         
                                                                                                    forKey:@"status"]
                                                          requestMethod:TWRequestMethodPOST];
                
				// Set the account used to post the tweet.
                [postRequest setAccount:twitterAccount];
                
				// Perform the request created above and create a handler block to handle the response.
				[postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    if ([urlResponse statusCode] != 200 || error != nil)
                        [self performSelectorOnMainThread:@selector(tweetFailed) withObject:nil waitUntilDone:NO];
                    else
                        [self performSelectorOnMainThread:@selector(tweetSucceeded) withObject:nil waitUntilDone:NO];
				}];
            };
			// Get the list of Twitter accounts.
            NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
            if ([accountsArray count] < 1) {
                [HOErrorHandler showWarningAlert:@"HO_ERROR_NO_TWITTER_ACOUNT_IN_SETTINGS"];
                [_spinner performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
            }
            else if ([accountsArray count] == 1)
                tweetBlock([accountsArray objectAtIndex:0]);
            else {
                [_accountListView release];
                _accountListView = [[TwitterAccountListViewController alloc] initWithAccounts:accountsArray];
                _accountListView.onTwitterAccountSelect = tweetBlock;
                [_accountListView show];
            }
        }
        else
            [self performSelectorOnMainThread:@selector(tweetFailed) withObject:nil waitUntilDone:NO];
	}];
}

// fallback
- (void) sendTweetWithExternalLibrary {
    [_twitter sendUpdate:_tweetMessage];
}

- (void) showDialog {
    [(DDSocialDialog*)self.view show];
}

- (void) tweet:(id)sender {
    [_spinner startAnimating];
    if ([TWRequest class] != nil) {
        if ([TWTweetComposeViewController canSendTweet])
            [self sendTweetWithNativeFramework];
        else {
            [HOErrorHandler showWarningAlert:@"HO_ERROR_NO_TWITTER_ACOUNT_IN_SETTINGS"];
            [_spinner stopAnimating];
        }
    } else {
        UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_twitter delegate:self];

        if (controller != nil) {
            if(_delegate && [_delegate conformsToProtocol:@protocol(TwitterPopupDelegate)])
                if ([_delegate respondsToSelector:@selector(shouldOpenAuthorizationDialog:)])
                    [_delegate shouldOpenAuthorizationDialog:controller];
        } else
            [self sendTweetWithExternalLibrary];
    }
}

- (void) tweetSucceeded {
    [_spinner setHidden:NO];
    [_spinner stopAnimating];
    [_tweetButton setEnabled:NO];
    [_tweetButton setAlpha:0.4];
    [_messageTextView setText:NSLocalizedString(@"HO_TWITTER_CONFIRMATION", nil)];
}

- (void) tweetFailed {
    [_spinner stopAnimating];
    [_tweetButton setEnabled:YES];
    [_tweetButton setAlpha:1];
    [_messageTextView setText:NSLocalizedString(@"HO_ERROR_TWITTER", nil)];
}

#pragma mark - DDSocialDialogDelegate methods

- (void) socialDialogDidCancel:(DDSocialDialog *)socialDialog {
    [self release];
}

#pragma mark - SA_OAuthTwitterControllerDelegate methods

- (void) OAuthTwitterController:(SA_OAuthTwitterController *)controller authenticatedWithUsername:(NSString *)username {
	[self showDialog];
}

- (NSString *) cachedTwitterOAuthDataForUsername:(NSString *)username {
    NSLog(@"cachedTwitterOAuthDataForUsername get: %@", [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"]);
    return [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"];    
}

- (void) storeCachedTwitterOAuthData:(NSString *)data forUsername:(NSString *)username {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults setObject: data forKey: @"authData"];
    [defaults synchronize];
    NSLog(@"cachedTwitterOAuthDataForUsername set: %@", [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"]);
}

#pragma mark -  MGTwitterEngineDelegate methods
- (void) requestSucceeded:(NSString *)connectionIdentifier {
    [self tweetSucceeded];
}

- (void) requestFailed:(NSString *)connectionIdentifier withError:(NSError *)error {
    NSLog(@"connectionIdentifier %@",connectionIdentifier);
    [self tweetFailed];
}

@end
