//
//  TwitterAccountListViewController.h
//  HelliOS
//
//  Created by Sergio Kunats on 3/19/12.
//  Copyright (c) 2012 Chugulu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>
#import "DDSocialDialog.h"

@interface TwitterAccountListViewController : UITableViewController<DDSocialDialogDelegate> {
    NSArray*    _accounts;
}

@property (nonatomic, copy) void (^onTwitterAccountSelect)(ACAccount *);

- (id)initWithAccounts:(NSArray *)accounts;
- (void)show;

@end
