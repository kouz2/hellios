/**
 *  @file		HOCacheObject.h
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		5/10/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *
 *  @brief This object provides automatic implementation of the NSCoding protocol.
 *         This object is designed to be subclassed.
 *         By sublassing this object, you allow your custom object
 *         to be archived. This class uses introspection through your
 *         custom object to get back any property when inserting in the cache,
 *         and to retreive them when unarchiving your object from the cache.
 */

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import "HOCache.h"


#pragma mark - Interface delcaration


@interface HOCacheObject : NSObject <NSCoding> 
{
    NSString *_objectKey;
}


#pragma mark - Properties


/**
 * @brief The object key in the cache.
 *        This key is used to retrieve the object in the cache
 */
@property (nonatomic, readonly) NSString *objectKey; 


#pragma mark - Methods


/**
 * @brief Insert the object in the cache with the given key
 *
 * @param NSString *key : The key to retrieve the object in the cache
 */
- (void) insertIntoCacheForKey:(NSString*)key;


/**
 * @brief Remove the object from the cache
 */
- (void) removeFromCache;

@end
