/**
 *  @file		HOCacheObject.m
 *  @brief		HelliOS_Components_Test 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		5/10/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 */

#import "HOCacheObject.h"

@interface HOCacheObject (Private) 

- (void) _addAllObservers;
- (void) _removeAllObservers;

@end

@implementation HOCacheObject

@synthesize objectKey = _objectKey;

#pragma mark - Initialization

- (id) init
{
    self = [super init];
    
    if (self) 
    {
  //      [self _addAllObservers];
    }
    
    return self;
}

#pragma mark - Memory management

- (void) dealloc
{
    //   [self _removeAllObservers];
    HO_SAFE_RELEASE(_objectKey);
    [super dealloc];
}

#pragma mark - Public

- (void) insertIntoCacheForKey:(NSString*)key
{
    [self removeFromCache];
    _objectKey = [key retain];
    [HOCache cacheObject:self forKey:_objectKey];
}

- (void) removeFromCache
{
    [HOCache removeCachedObjectForKey:_objectKey];
    HO_SAFE_RELEASE(_objectKey);
}

#pragma mark - Observing (Private)

- (void)didChangeValueForKey:(NSString *)key
{
    if (_objectKey)
        [self insertIntoCacheForKey:_objectKey];
    [super didChangeValueForKey:key];
}

- (void) _addAllObservers
{
    u_int count;
    objc_property_t * ivars = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i < count ; i++)
    {
        NSString *propertyName  = [NSString stringWithUTF8String:property_getName(ivars[i])];
        
        [self addObserver:self 
               forKeyPath:propertyName 
                  options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld 
                  context:NULL];        
    }
    
    free(ivars); 
}

- (void) _removeAllObservers
{
    u_int count;
    objc_property_t * ivars = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i < count ; i++)
    {
        NSString *propertyName  = [NSString stringWithUTF8String:property_getName(ivars[i])];
        [self removeObserver:self forKeyPath:propertyName];
    }
    
    free(ivars);
}

#pragma mark - NSCoding protocol methods

- (void)encodeWithCoder:(NSCoder *)coder
{    
	u_int count;
    Ivar* ivars = class_copyIvarList([self class], &count);
    
    for (int i = 0; i < count ; i++)
    {
        NSString *propertyName  = [NSString stringWithUTF8String:ivar_getName(ivars[i])];

        if ([[[self valueForKey:propertyName] class] conformsToProtocol:@protocol(NSCoding)])
        {                            
            [coder encodeObject:[self valueForKey:propertyName] forKey:propertyName];
        }
    }

    free(ivars);

    [coder encodeObject:[super valueForKey:@"objectKey"] forKey:@"objectKey"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    u_int count;
    Ivar* ivars = class_copyIvarList([self class], &count);

    for (int i = 0; i < count; i++)
    {
        NSString *propertyName  = [NSString stringWithUTF8String:ivar_getName(ivars[i])];
       
        if ([decoder containsValueForKey:propertyName]) 
        {
            id object = [decoder decodeObjectForKey:propertyName];

//            if ([object isKindOfClass:[HOCacheObject class]] && ((HOCacheObject*)object).objectKey) 
//            {
//                object = [HOCache cachedObjectForKey:((HOCacheObject*)object).objectKey];
//            }
//            else if ([object isKindOfClass:[NSArray class]])
//            {
//                NSMutableArray *newValues = [NSMutableArray array];
// 
//                for (id anObject in object) 
//                {
//                    if (![anObject isKindOfClass:[HOCacheObject class]]) 
//                    {
//                        break;
//                    }
//                    
//                    if (((HOCacheObject*)anObject).objectKey) 
//                    {
//                        [newValues addObject:[HOCache cachedObjectForKey:((HOCacheObject*)anObject).objectKey]];
//                    }
//                }
//                
//                object = newValues;
//            }
            
            [self setValue:object forKey:propertyName];
        }
    }

    free(ivars);

    [super setValue:[decoder decodeObjectForKey:@"objectKey"] forKey:@"objectKey"];
 //   [self _addAllObservers];
    return self;
}

@end
