/**
 @file		HOCache.h
 @brief		HOCache 
 @author	Olivier Thierry
 @version	1.0
 @date		25/02/2011
 
 Copyright Chugulu corp 2009-2011. All rights reserved.

 Cache module for storing objects
*/

@interface HOCache : NSObject {}

/**
 @brief this method cache an object with for a given key

 @param NSObject *object   : the object to cache
 @param NSString *key      : the key to retreive the object in cache
*/
+ (void) cacheObject:(id<NSObject, NSCoding>)object forKey:(NSString*)name;


/**
 @brief this method return a cached object with the given key
 
 @param NSString *key      : the key to retreive the object in cache 
 
 @return id                : the cached object 
*/
+ (id)	 cachedObjectForKey:(NSString*)key;


/**
 @brief this method return an array of cached objects having the given key prefix
 
 @param NSString *keyPrefix : the key prefix to retreive objects in cache 
 
 @return NSArray*           : An array containing fetched objects 
 */
+ (NSArray*) cachedObjectsWithKeyPrefix:(NSString*)keyPrefix;


/**
 @brief this method return an array of cached object keys
 
 @return NSArray*           : An array containing cached object keys
 */
+ (NSArray*) cachedObjectKeys;


/**
 @brief this method remove a cached object with the given key
 
 @param NSString *key      : the key to retreive the object in cache 
*/
+ (void) removeCachedObjectForKey:(NSString*)key;


/**
 @brief this method remove cached objects with given keys
 
 @param NSArray *keys      : the keys array to retreive the objects in cache 
 */
+ (void) removeCachedObjectsForKeys:(NSArray*)keys;


/**
 @brief this method remove cached objects with keys that contains the given prefix
 
 @param NSString *keyPrefix      : the key prefix (e.g: @"mp3_")
 */
+ (void) removeCachedObjectsWithKeyPrefix:(NSString*)keyPrefix;


/**
 @brief this method remove ALL cached object
*/
+ (void) clear;

@end
