/*******************************************************************************
 *  @file		HONotificationCenterHTTPClientBase.m
 *  @brief		HelliOs 
 *  @author		Sergio Kunats
 *  @version	1.0
 *  @date		7/26/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HONotificationCenterHTTPClientBase.h"
#import "HONotification.h"
#import "HONotificationCenter.h"

@implementation HONotificationCenterHTTPClientBase

#pragma mark - NSObject Life Cycle

- (id) initWithDeliveryDelegate:(HONotificationCenter*)center {
    if ((self = [super init])) {
        _deliveryDelegate = [center retain];
    }
    return self;
}

- (void) dealloc {
    [_deliveryDelegate release], _deliveryDelegate = nil;
    [super dealloc];
}

#pragma mark - Client Life Cycle

- (NSURL*) notificationsURL {
    if (![HelliOS sharedInstance].settings.notificationsURL)
        @throw [NSException exceptionWithName:@"HONotificationCenterInitializationException" reason:@"HOSettings.notificationsURL is nil.\n " \
                "Please initialize HelliOS properly by passing" \
                "a valid notificationsURL to the settings parameters\n" \
                "(key = kHOSettingsNotificationsURL)" userInfo:nil];

    return [HelliOS sharedInstance].settings.notificationsURL;
}

- (void) pull {
    @throw [NSException exceptionWithName:@"HONotificationCenterHTTPClientException" reason:@"You MUST implement your own [- (void) pull] method" userInfo:nil];
}

- (void) deliver:(NSArray*)objects {
    for (NSDictionary *payload in objects) 
	{
        // check if the notification has a parrent and if its true if the parent notification has been displayed and if the notification has not already been displayed
        // or just if the notification has not already been displayed (Assuming there's no parent)
        /**<@todo: correct parrent param on blindtest*/
		if (([payload objectForKey:@"parrent"] && [_deliveryDelegate didDeliverNotificationWithID:[payload objectForKey:@"parrent"]] && ![_deliveryDelegate didDeliverNotificationWithID:[payload objectForKey:@"ID"]]) || ![_deliveryDelegate didDeliverNotificationWithID:[payload objectForKey:@"id"]])
		{
            HONotification *notification = [[HONotification alloc] initWithPayload:payload];
            [_deliveryDelegate deliver:notification];
		}
	}
}

@end
