/*******************************************************************************
 *  @file		HONotificationCenterDelegateProtocol.h
 *  @brief		HelliOS 
 *  @author		Sergio Kunats
 *  @version	1.0
 *  @date		7/25/11
 *
 *  Copyright 	Chugulu 2009-2011. All rights reserved.
 *******************************************************************************/

@class HONotification;

@protocol HONotificationCenterDelegateProtocol <NSObject>

- (void) didReceiveProviderNotification:(HONotification*)notification;

@end
