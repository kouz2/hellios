/*******************************************************************************
 *  @file		HONotificationViewController.m
 *  @brief		HelliOS 
 *  @author		Olivier THIERRY
 *  @version	1.0
 *  @date		6/8/11
 *
 *  Copyright 	__MyCompanyName__ 2009-2011. All rights reserved.
 *******************************************************************************/

#import "HONotificationViewController.h"

@implementation HONotificationViewController

@synthesize notificationTitleLabel        = _notificationTitleLabel,
            notificationDescriptionLabel  = _notificationDescriptionLabel,
            notificationContainerView     = _notificationContainerView,
            onAccept                      = _onAccept,
            onDismiss                     = _onDismiss,
            notification                  = _notification,
            notificationAcceptButtonLabelText=_notificationAcceptButtonLabelText,
            notificationAcceptButton      = _notificationAcceptButton;

#pragma mark - 
#pragma mark Initialization

- (id) initWithNotification:(HONotification*)notification;
{
    self = [super init];
    if (self) 
    {
        _notificationAcceptButtonLabelText = nil;
        _notification = [notification retain];
    }
    return self;
}

+ (id) controllerWithNotification:(HONotification*)notification
{
    return [[[self alloc] initWithNotification:notification] autorelease];
}

- (void)dealloc
{
    [_notificationAcceptButtonLabelText release], _notificationAcceptButtonLabelText = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - 
#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    _notificationTitleLabel.text       = _notification.title;
    _notificationDescriptionLabel.text = _notification.message;

    if (_notificationAcceptButtonLabelText != nil)
        [_notificationAcceptButton setTitle:_notificationAcceptButtonLabelText forState:UIControlStateNormal];

    CGSize requiredSized = [_notificationAcceptButton.titleLabel.text sizeWithFont:_notificationAcceptButton.titleLabel.font];
    
    _notificationAcceptButton.frame  = CGRectMake(_notificationAcceptButton.frame.origin.x, 
                                                  _notificationAcceptButton.frame.origin.y,
                                                  requiredSized.width + 20, 
                                                  _notificationAcceptButton.frame.size.height);
    
    _notificationAcceptButton.center = CGPointMake(_notificationContainerView.frame.size.width/2, _notificationAcceptButton.center.y);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return [[HelliOS sharedInstance].settings.supportedInterfaceOrientations containsObject:[NSNumber numberWithInt:interfaceOrientation]];
}

#pragma mark - 
#pragma mark Display methods

- (CGAffineTransform) getAffineTransformForCurrentInterfaceOrientation {
	
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	
	if (orientation == UIInterfaceOrientationLandscapeLeft) {
		return CGAffineTransformMakeRotation(M_PI*1.5);
	} 
	else if (orientation == UIInterfaceOrientationLandscapeRight) {
		return CGAffineTransformMakeRotation(M_PI/2);
	} 
	else if (orientation == UIInterfaceOrientationPortraitUpsideDown) {
		return CGAffineTransformMakeRotation(-M_PI);
	}
	
	return CGAffineTransformIdentity;
}

- (void)show 
{    
    [self retain];
    
	if (!self.view.superview) 
    {
		UIWindow *window	= [UIApplication sharedApplication].keyWindow;
		
		if (!window) {
			window			= [[UIApplication sharedApplication].windows objectAtIndex:0];
		}
		
		self.view.transform		= [self getAffineTransformForCurrentInterfaceOrientation];
		self.view.center        = window.center;
        
		[window addSubview:self.view];		
	}
	
	self.notificationContainerView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
	[UIView beginAnimations:@"show" context:nil];
	[UIView setAnimationDuration:0.20f];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	{
		self.view.alpha = 1.0f;
		self.notificationContainerView.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
	}
	[UIView commitAnimations];	
	
}

- (void)hide 
{	
	[UIView beginAnimations:@"hide" context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView setAnimationDuration:0.5f];
	{
		self.view.alpha = 0.0f;
	}
	[UIView commitAnimations];	
}


#pragma mark - 
#pragma mark View controller actions

- (IBAction) dismiss 
{
    [self hide];
    if (_onDismiss)
    {
        [_onDismiss perform];
    }
    [self release];
}

- (IBAction) accept 
{
    [self hide];
    if (_onAccept)
    {
        [_onAccept perform];
    }
    [self release];
}

@end
