//
//  HONotification.m
//  HelliOS
//
//  Created by Olivier THIERRY on 4/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HONotification.h"

@implementation HONotification

@synthesize ID				  = _ID,
			title			  = _title,
			message			  = _message, 
            publishDate       = _publishDate, 
            TTL               = _TTL,
			type			  = _type,
			attachedObjects	  = _attachedObjects;
@synthesize alreadyRead;


- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@> ID : %@, Title: %@, Message: %@, PublishDate: %@, TTL: %f, Type: %d, attachedObjects: %@", NSStringFromClass([self class]), _ID, _title, _message, _publishDate, _TTL, _type, _attachedObjects];
}

#pragma mark - NSObject Life Cycle

- (id) initWithPayload:(NSDictionary*)payload {
    if ((self = [super init])) {
        self.title				= [payload objectForKey:@"title"];
        self.message			= [payload objectForKey:@"description"];
        self.ID					= [payload objectForKey:@"id"];
        id ttl = [payload objectForKey:@"ttl"];
        self.TTL                = [ttl isKindOfClass:[NSNull class]] ? 0 : [ttl intValue];
        self.type				= [[payload objectForKey:@"type"] intValue];
        self.publishDate        = [payload objectForKey:@"published_on"];/**<@note based on Playboy spots payload -> should be normalized*/

        if ([payload objectForKey:@"payload"] != [NSNull null])
            self.attachedObjects = [payload objectForKey:@"payload"];
        [self setAlreadyRead:NO];
    }
    return self;
}

- (id) initWithTitle:(NSString*)title
			 message:(NSString*)message
{
	self = [super init];
	
	if (self) {
		self.title		= title;
		self.message	= message;
        [self setAlreadyRead:NO];
	}
	
	return self;	
}


#pragma mark - Memory

- (void) dealloc 
{
	[_title release],			_title = nil;
	[_message release],			_message = nil;
	[_attachedObjects release], _attachedObjects = nil;
	[super dealloc];
}


#pragma mark - Accessors

-(void) setAlreadyRead:(BOOL)alreadyRead{
    [_isAlreadyRead release];
    _isAlreadyRead = nil;
    _isAlreadyRead = [[NSNumber numberWithBool:alreadyRead] retain];
}
-(BOOL) isAlreadyRead{
    return [_isAlreadyRead boolValue];
}

@end
