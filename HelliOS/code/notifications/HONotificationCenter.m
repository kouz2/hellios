//
//  HONotificationCenter.m
//  HelliOS
//
//  Created by Olivier THIERRY on 5/3/11.
//  Copyright 2011 Chugulu. All rights reserved.
//

#import "HONotificationCenter.h"

NSString* const kHelliOSNotificationsDelivered = @"hellios.notifications.delivered";

@interface HONotificationCenter (Private)

- (void)			_pull;

@end

@implementation HONotificationCenter

@synthesize delegate=_delegate;
@synthesize notificationHttpClient=_notificationHttpClient;

#pragma mark --
#pragma mark Initialization

- (id) init
{
	self = [super init];
	
	if (self) 
	{
		_deliveredNotificationIDs = [[NSMutableArray alloc] init];
		
		if ([[NSUserDefaults standardUserDefaults] arrayForKey:kHelliOSNotificationsDelivered]) {
			[_deliveredNotificationIDs addObjectsFromArray:[[NSUserDefaults standardUserDefaults] arrayForKey:kHelliOSNotificationsDelivered]];
		}
		else {
			[[NSUserDefaults standardUserDefaults] setObject:_deliveredNotificationIDs forKey:kHelliOSNotificationsDelivered];
			[[NSUserDefaults standardUserDefaults] synchronize];
		}
	}
	
	return self;
}

#pragma mark --
#pragma mark Singleton accessor

+ (HONotificationCenter*) sharedNotificationCenter
{
	static HONotificationCenter *sharedInstance = NULL;
	
	if (sharedInstance == NULL) {
		sharedInstance = [[HONotificationCenter alloc] init];
	}
	
	return sharedInstance;
}

#pragma mark --
#pragma mark Public methods

- (void) pull
{
    if (_pullTimer) 
    {
        [self schedulePullWithTimeInterval:[_pullTimer timeInterval]];
    }
    
    [self _pull];
}

- (void)				  schedulePullWithTimeInterval:(NSTimeInterval)timeInterval
{
	if (_pullTimer) [self unschedulePull];	
	_pullTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(_pull) userInfo:nil repeats:YES];
}

- (void)				  unschedulePull
{
	[_pullTimer invalidate];
	_pullTimer = nil;
}

- (BOOL) didDeliverNotificationWithID:(NSString*)ID
{
	return [_deliveredNotificationIDs containsObject:ID];
}

- (void) deliver:(HONotification*)notification
{
	if (notification.ID)
	{
		[_deliveredNotificationIDs addObject:notification.ID];
		[[NSUserDefaults standardUserDefaults] setObject:_deliveredNotificationIDs forKey:kHelliOSNotificationsDelivered];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
	if ([_delegate respondsToSelector:@selector(didReceiveProviderNotification:)])
		[_delegate didReceiveProviderNotification:notification];
}

#pragma mark --
#pragma mark Private methods

- (void)			_pull
{
    if ([_notificationHttpClient respondsToSelector:@selector(pull)])
        [_notificationHttpClient pull];
}

#pragma mark --
#pragma mark Memory managment

- (void)dealloc 
{
	// singleton
}


@end
