//
//  HONotificationView.h
//  HelliOS
//
//  Created by Olivier THIERRY on 4/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HONotification.h"

#define kNotificationFrame CGRectMake(0, 0, 200, 260)

@interface HONotificationView : UIView <UIAlertViewDelegate> {
@private
    // View components
    UIView                      *_notificationContainerView;
    UILabel                     *_notificationTitleLabel;
    UITextView                  *_notificationDescriptionLabel;
    UIButton                    *_notificationAcceptButton;

	HONotification				*_notification;	

	HODelegate					*_onAccept;
	HODelegate					*_onDismiss;
}

@property (nonatomic, retain, readonly) IBOutlet  UIView          *notificationContainerView;
@property (nonatomic, retain, readonly) IBOutlet  UILabel         *notificationTitleLabel;
@property (nonatomic, retain, readonly) IBOutlet  UITextView      *notificationDescriptionLabel;
@property (nonatomic, retain, readonly) IBOutlet  UIButton        *notificationAcceptButton;

@property (nonatomic, retain, readonly) HONotification            *notification;

// event delegates
@property (nonatomic, retain)			HODelegate                *onAccept;
@property (nonatomic, retain)			HODelegate                *onDismiss;

- (id)	 initWithNotification:(HONotification*)notification;

- (void) show;
- (void) hide;

- (IBAction) dismiss;
- (IBAction) accept;

@end
