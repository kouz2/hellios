//
//  HONotificationView.m
//  HelliOS
//
//  Created by Olivier THIERRY on 4/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HONotificationView.h"

@implementation HONotificationView

@synthesize notification                  = _notification,
			onAccept                      = _onAccept,
			onDismiss                     = _onDismiss,
            notificationTitleLabel        = _notificationTitleLabel,
            notificationDescriptionLabel  = _notificationDescriptionLabel,
            notificationContainerView     = _notificationContainerView,
            notificationAcceptButton      = _notificationAcceptButton;

- (id)initWithNotification:(HONotification *)notification
{    
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"HONotificationView" owner:self options:nil];
    
    for (id object in nibObjects) 
    {
        if ([object isKindOfClass:[self class]]) 
        {
            self                                   = [object retain];
            self.notificationTitleLabel.text       = notification.title;
            self.notificationDescriptionLabel.text = notification.message;
            _notification                          = [notification retain];
        }
    }
    
    return self;
}

- (void) drawRect:(CGRect)rect
{
    CGSize requiredSized = [_notificationAcceptButton.titleLabel.text sizeWithFont:_notificationAcceptButton.titleLabel.font];
    
    _notificationAcceptButton.frame  = CGRectMake(_notificationAcceptButton.frame.origin.x, 
                                                  _notificationAcceptButton.frame.origin.y,
                                                  requiredSized.width + 20, 
                                                  _notificationAcceptButton.frame.size.height);
    
    _notificationAcceptButton.center = CGPointMake(self.center.x, _notificationAcceptButton.center.y);
}

#pragma mark - 
#pragma mark Display methods

- (CGAffineTransform) getAffineTransformForCurrentInterfaceOrientation {
	
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	
	if (orientation == UIInterfaceOrientationLandscapeLeft) {
		return CGAffineTransformMakeRotation(M_PI*1.5);
	} 
	else if (orientation == UIInterfaceOrientationLandscapeRight) {
		return CGAffineTransformMakeRotation(M_PI/2);
	} 
	else if (orientation == UIInterfaceOrientationPortraitUpsideDown) {
		return CGAffineTransformMakeRotation(-M_PI);
	}
	
	return CGAffineTransformIdentity;
}

- (void)show {
    
	if (!self.superview) 
    {
		UIWindow *window	= [UIApplication sharedApplication].keyWindow;
		
		if (!window) {
			window			= [[UIApplication sharedApplication].windows objectAtIndex:0];
		}
		
		self.transform		= [self getAffineTransformForCurrentInterfaceOrientation];
		self.center         = window.center;
        
		[window addSubview:self];		
	}
	
	_notificationContainerView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
	[UIView beginAnimations:@"show" context:nil];
	[UIView setAnimationDuration:0.20f];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	
	{
		self.alpha = 1.0f;
		_notificationContainerView.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
	}
	[UIView commitAnimations];	
	
}

- (void)hide {
	
	[UIView beginAnimations:@"hide" context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView setAnimationDuration:0.5f];
	{
		self.alpha = 0.0f;
	}
	[UIView commitAnimations];	
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex 
{
	if (buttonIndex == 1 && _onAccept) 
    {
        [_onAccept perform];
	}
	else if (_onDismiss)
    {
        [_onDismiss perform];	        
	}
}

- (IBAction) dismiss 
{
    [self hide];
    if (_onDismiss)
    {
        [_onDismiss perform];
    }
}

- (IBAction) accept 
{
    [self hide];
    if (_onAccept)
    {
        [_onAccept perform];
    }
}

- (void)dealloc 
{
    HO_SAFE_RELEASE(_notification);
    [super dealloc];
}


@end
